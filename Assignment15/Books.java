package Assignments.assignments.Assignment15;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

interface Books {

    public void getAuthor();
    public void getTitle();
    public void display();
    // Thieu interface loc cac String co ' t ' dau tien

   public static void main(String[] args){
       ArrayList<String> list = new ArrayList<String>();
       list.add("Think and Grow Rich");
       list.add("Global Economy");
       list.add("The Intelligent Investor");
       list.add("You Can Win");
       list.add("The Art of War");
       list.add("Habit Makes Perfect");
       list.add("Gulliver's Travel");
       list.add("The Black Sheep");
       list.add("War and Peace");
       list.add("The Odyssey");
       System.out.println("\nThe list of books :");
       for (int i = 0; i < list.size(); i++) {
        System.out.println(list.get(i));
    }
   }
}
