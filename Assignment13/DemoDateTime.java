package Assignments.assignments.Assignment13;

import java.util.Formatter;
import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DemoDateTime {

    private String cname;
    private String policyType;
    private String policyStart;
    private String dateOfBirth = "09/12/1960";

    public DemoDateTime(String cname, String policyType, String policyStart){
        this.cname = cname;
        this.policyStart = policyStart;
        this.policyType = policyType;
    }

    public void setCName(String cname){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter customer name :");
         cname = input.next();
    }
     public String getCName(){
        return cname;
    }

    public void setPolicyType(String policyType){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter policy type :");
         policyType = input.next();
    }
     public String getPolicyType(){
        return policyType;
    }

    public void setPolicyStart(String policyName){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter policy start :");
         policyStart = input.next();
    }
     public String getPolicyStart(){
        return policyStart;
    }

    public void getAge(String dateOfBirth){
        this.dateOfBirth = dateOfBirth;
        formatter = new SimpleDateFormat("MM/dd/yyyy");
       System.out.println("Age = " +(formatter.parse(dateOfBirth)));
    }
    
}
