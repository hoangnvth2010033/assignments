package Assignments.assignments.Assignment12;

import java.util.Scanner;

public class Bank extends Account{

     protected String bankID;
     protected String bankName;
     protected String branch;

     public Bank(String customerName , String accountNumber, double balance, String bankID, String bankName, String branch){
         super(customerName, accountNumber, balance);
         this.bankID = bankID;
         this.bankName = bankName;
         this.branch = branch;
     }

    
     public void setBankID(String bankID){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter Bank ID :");
         bankID = input.next();
    }
     public String getBankID(){
        return bankID;
    }


    public void setBankName(String bankName){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter Bank name :");
         bankName = input.next();
    }
     public String getBankName(){
        return bankName;
    }
   

    public void setBranch(String branch){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter branch :");
         branch = input.next();
    }
     public String getBranch(){
        return branch;
    }


    public void createAccount(){

        
        System.out.println("\nEnter bank details :\n ====================================\n");
        setBankID("23");//bien tam thoi
        setBankName("hello");
        setBranch("hi");

        System.out.println("\nEnter account details :\n ====================================\n");
        setCustName("nam");// bien tam thoi
        setAccNumber("12");
        setBalance(23);

    }

    public void displayAccountDetails(){

        System.out.println("\nBank details :\n ====================================\n");
        System.out.println(" Bank { name = " +getBankName()+" |  ID = " +getBankID()+" | branch = " +getBranch()+ " }");
        System.out.println(" Account { Username = " +getCustName() +"  |  Account number = " +getAccNumber() + "  }");

    }

    public void withdraw(double amount){// rut tien

        setAccNumber("12");// nhap so TK
        Scanner read = new Scanner(System.in);

        try{
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter amount of money to withdraw :");
         amount = input.nextDouble();
         if (amount> balance) {
			throw new ArithmeticException(" The withdraw is greater than balance -- Error !"); /// Muon su dung try catch thi ??
		}
        }catch(NumberFormatException ex){
            System.out.println("\nEnter number; not a string !");
        }
        finally{
            System.out.println("Finished!");
        }
        

    }


    public void deposit(double amount){// gui tien

        setAccNumber("12");// nhap so TK
        try{
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter amount of money to deposit :");
         amount = input.nextDouble();
        }catch(NumberFormatException ex){
            System.out.println("\nEnter number; not a string !");
        }

    }

    
}
